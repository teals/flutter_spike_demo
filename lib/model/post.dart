class Post{
  int userId;
  int postId;
  String title;
  String content;
  bool isDetail = false;
  Post(this.userId, this.postId, this.title, this.content);

  Post.fromJson(Map json)
    : userId = json['userId'],
      postId = json['id'],
      title = json['title'],
      content = json['body'];
}
