
import 'package:flutter_spike_demo/bloc/post_bloc.dart';
import 'package:flutter/material.dart';
import '../bloc/post_provider.dart';

class PostPage extends StatefulWidget {
  @override
  _PostPageState createState() => _PostPageState();
}

// 스냅샷이 뭐지?
class _PostPageState extends State<PostPage>
{
  Widget _buildListTile (AsyncSnapshot snapshot, int index)
  {
    var userId = snapshot.data[index].userId;
    var postId = snapshot.data[index].postId;
    var title = snapshot.data[index].title;
    var content = snapshot.data[index].content;
    var isDetail = snapshot.data[index].isDetail;
    return Card(
      child: Column
      (
        children: <Widget>[ 
          ListTile(
          leading: Text("$userId"),
          title: Padding(
            padding: const EdgeInsets.only(bottom: 5),
            child: Text("$title"),
          ),
          
          subtitle: isDetail == true ? Text("$content"):Text(content,maxLines: 2,),
          ),
          isDetail == true ?
            Row(
              mainAxisAlignment:MainAxisAlignment.end,
              children: <Widget>[

                // Padding(
                //   padding: const EdgeInsets.only(left: 10,right: 10),
                //   child: InkWell(
                //     onTap: (){},
                //     child: Container(child: const Text("댓글"),
                //   ),
                //   ),
                // ),
                Padding(
                  padding: const EdgeInsets.only(left: 10,right: 10),
                  child: InkWell(
                    onTap: (){ 
                        PostProvider.of(context).deletePost(index, snapshot);
                      },
                    child: Container(child: const Text("삭제"),
               ),
                  ),
                ),
              ],
            )
           :  Text("",textScaleFactor: 0,),
           Padding(
             padding: const EdgeInsets.only(bottom: 10),
             child: InkWell(
                  onTap: (){
                    PostProvider.of(context).setDetailMode(index, snapshot);
                  },
                  child: Container(child: Text(isDetail == true ? "줄이기" : "늘리기"),),
              ),
           ),
        ]
    )
      
    );
  }
  Widget build(BuildContext context)
  {
    final postBloc = PostProvider.of(context);

    return Center(
        child: Column(
          children: <Widget>[
            RaisedButton(
              color: Colors.blueAccent,
              child:Text("Load Data"),
              onPressed: (){
                postBloc.getPost();
              },
            ),
            Flexible(
              child: StreamBuilder (
                stream: postBloc.results,
                builder: (context, snapshot)
                {
                  PostProvider.of(context).renewlPostList();
                  if(!snapshot.hasData)
                    return(Text("데이터가 없습니다."));
                  else
                    return ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index) => _buildListTile(snapshot, index),
                    );
                },),)
          ],),);
  }
}