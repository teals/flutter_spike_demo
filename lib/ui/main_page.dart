import 'package:flutter/material.dart';
import 'package:http/http.dart';
import '../bloc/post_provider.dart';
import 'post_page.dart';
import '../bloc/post_provider.dart';
import '../bloc/post_bloc.dart';

class BasePage extends StatefulWidget {

  @override
  _BasePageState createState() => _BasePageState();
}

class _BasePageState extends State<BasePage>
{
  int _selectedIndex = 0;
  Widget postPage = PostPage();
  Widget _screen;
  @override
  void initState() {
    super.initState();
    _screen = postPage;
  }
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      if(index == 0)
        _screen = postPage;
      else
        _screen = Center(child: Text("빈칸"),);
    });
  }

  Widget build(BuildContext context)
  {
    return Scaffold(
      appBar: AppBar(title:Text("Bloc 리스트")),
      body : _screen,

      bottomNavigationBar: BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(IconData(57527, fontFamily: 'MaterialIcons')),
          title: Text('Post'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.business),
          title: Text('Temp'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.school),
          title: Text('Temp'),
        ),
      ],
      currentIndex: _selectedIndex,
      selectedItemColor: Colors.amber[800],
      onTap: _onItemTapped,
    ),
    
    floatingActionButton: _selectedIndex == 0 ? FloatingActionButton(
      onPressed: () {
        print("들어옴!");
        PostProvider.of(context).results.isEmpty.then((onValue){
          if(onValue == true)
            print("빔!");
          else
            print("안빔!");
        }
        );
      
        
      },
      child: Icon(IconData(57899, fontFamily: 'MaterialIcons')),
    ):null,
    );
  }
}