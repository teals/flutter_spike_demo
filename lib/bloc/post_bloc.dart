import 'dart:async';
import 'package:flutter/cupertino.dart';

import '../model/post.dart';
import '../repository/api.dart';

//블록은 항상 Provider를 통해서 사용
class PostBloc {
  final API api;
  List<Post> postList = null;
  StreamController <List<Post>> ctrl = StreamController.broadcast();

  Stream<List<Post>> get results => ctrl.stream.asBroadcastStream(); // 곧바로 스트림에 접근하지 않기위해 만듬

  PostBloc(this.api);

  void dispose()
  {
    ctrl.close();
  }

  void getPost()
  {
    api.getPosts().then((posts){
      postList = posts;
      ctrl.add(postList);
    });
  }
  void setDetailMode(int index, AsyncSnapshot snapshot)
  {
    postList[index].isDetail = !postList[index].isDetail;
    ctrl.add(postList);
  }
  void deletePost(int index, AsyncSnapshot snapshot)
  {
    postList.removeAt(index);
    ctrl.add(postList);
  }
  void renewlPostList()
  {
    ctrl.add(postList);
  }
}