import 'package:flutter_spike_demo/ui/main_page.dart';
import 'package:flutter_spike_demo/ui/post_page.dart' as prefix0;
import 'package:flutter/material.dart';
import 'ui/post_page.dart';
import 'bloc/post_provider.dart';
import 'bloc/post_bloc.dart';
import 'repository/api.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return PostProvider(
      postBloc: PostBloc(API()),
      child: MaterialApp(
        title: 'Flutter Bloc demoz',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: BasePage(),
        
      ),
    );
  }
}